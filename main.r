setwd("D:/marun.mis.7731") 
#to use spss sav files, added foreign package to r script.
library(foreign)

car_sales = read.spss("D:\\marun.mis.7731\\car_sales.sav", to.data.frame=TRUE)

head(car_sales)

table(car_sales$sales)

min(car_sales$sales)
max(car_sales$sales)

#get the average value 
mean(car_sales$sales)
#get the quantiles
quantile(car_sales$sales)
#get the standart deviation 
sd(car_sales$sales)
#get the correlation cofficient
cor(  
  x = car_sales$price,
  y = car_sales$engine_s,
  use = "complete",
  #method = c("pearson","kendall","spearman")
  method = "spearman"
)

descriptive_report <- summary(car_sales)

# Load the dplyr library
library(dplyr)
numerical_temp <- select(
  .data = car_sales,
  sales,
  price,
  engine_s,
  horsepow,
  width,
  length
)
# Inspect the results
head(numerical_temp)

# heterogeneous correlations in one matrix 
# pearson (numeric-numeric), 
# polyserial (numeric-ordinal), 
# and polychoric (ordinal-ordinal)
# x is a data frame with ordered factors 
# and numeric variables
library(polycor)
library(mvtnorm)
library(sfsmisc)

x <- numerical_temp[1:2]
y <- numerical_temp[3:6]
cor(x,y,use="complete")

# Correlations with significance levels
library(grid)
library(survival)
library(Formula)
library(ggplot2)
library(Hmisc)
rcorr(as.matrix(x), type="pearson") # type can be pearson or spearman
efficiency <- cor(numerical_temp,use = "complete")

print(efficiency)

#to output our data to csv file
write.csv(
  x = efficiency, 
  file = "marun.main.mis.7731.csv",
  row.names = TRUE
)

#Descriptive Statistics
#mean, sd, var, min, max, median, range, and quantile
sapply(car_sales, mean, na.rm=FALSE)
# mean,median,25th and 75th quartiles,min,max
marun_731_descriptive_statistics <- summary(car_sales)
# write descriptive statistics result in csv file
write.csv(
  x = marun_731_descriptive_statistics,
  file = "marun.731.descriiptive_statistics.csv",
  row.names = TRUE  
)

#ANOVA
#source: http://www.statmethods.net/stats/anova.html
#one way Anova (Completely Randomized Design)
anova_1 <- aov(age~iq, data=numerical_temp)

anova_1_result <- summary(anova_1)


# Randomized Block Design (B is the blocking factor) 
anova_2 <- aov(sales ~ price + engine_s, data=numerical_temp)

#Two way factorial design 
anova_3  <- aov(sales ~ price + engine_s + price:engine_s, data= numerical_temp)
anova_4 <- aov(sales ~ price * engine_s, data = numerical_temp) #same thing

write.csv(
  x  = anova_1_result, 
  file = "marun.mis.731.anova_1.csv",
  row.names = TRUE
)

#load the ggplot2 library 
library(ggplot2)

# Create a frequency bar chart
ggplot(
  data = numerical_temp, 
  aes(x = sales)) + 
  geom_bar() +
  ggtitle("Count of Sales") +
  xlab("length Type") +
  ylab("Count of Cars")




